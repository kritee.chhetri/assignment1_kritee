import java.util.Scanner;

public class Assignment16 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		 System.out.println("Input 1st integer: ");
		 int first = scan.nextInt();

		 System.out.println("Input 2nd integer: ");
		 int second = scan.nextInt();
		 
		 int sum = first+second;
		 int difference = first-second;
		 int product = first*second;
		 double average = (first+second)/2;
		 int distance = first - second;
		 int maximum = Math.max(first, second);
		 int minimum = Math.min(first, second);
		 
				 
		 System.out.println("Sum of two integers: "+sum);
		 System.out.println("Difference of two integers: "+difference);
		 System.out.println("Product of two integers: "+product);
		 System.out.println("Average of two integers: "+average);
		 System.out.println("Distance of two integers: "+distance);
		 System.out.println("Max  integer: "+maximum);
		 System.out.println("Min integer: "+minimum);

	}

}
