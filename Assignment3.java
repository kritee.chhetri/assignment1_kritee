
//created class
public class Assignment3 {
	
	//create string x
	String backAround(String x) {
		
		//taking last string and adding it front and back
		String back = x.substring(x.length()-1,x.length());
		String word = back+x+back;
		return word;
	}

	public static void main(String[] args) {
		
		//created new object
		Assignment3 name = new Assignment3();
		
		//printing out results
		System.out.println(name.backAround("cat"));
		System.out.println(name.backAround("Hello"));
		System.out.println(name.backAround("a"));

	}

}
