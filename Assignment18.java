import java.util.Arrays;

public class Assignment18 {

	public static void main(String[] args) {
		int array[]= {1,2,3,4,5};
		int IndexPosition = 3;
		int newvalue = 1;
		System.out.println("Old Array: "+Arrays.toString(array));
		
		for(int i = array.length-1; i > IndexPosition; i --) {
			array[i]=array[i-1];
		}
		array[IndexPosition] = newvalue;
		System.out.println("New Array: "+Arrays.toString(array));

	}

}
