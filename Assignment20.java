
public class Assignment20 {

	public static void main(String[] args) {
		String str1= "Stephen Edwin King";
		String str2= "Walter Winchell";
		String str3= "Mike Royko";
		
		boolean str1equalsstr2 = str1.equals(str2);
		boolean str1equalsstr3 = str1.equals(str3);
		
		System.out.println("\""+str1+"\"equals\""+str2+"\"? "+str1equalsstr2);
		System.out.println("\""+str1+"\"equals\""+str3+"\"? "+str1equalsstr3);
		

	}

}
