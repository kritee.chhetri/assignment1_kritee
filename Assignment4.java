
public class Assignment4 {

	String Hello(String name) {
		return "Hello "+name+"!" ;
	}

	public static void main(String[] args) {
		Assignment4 hello = new Assignment4();
		System.out.println(hello.Hello("Bob"));
		System.out.println(hello.Hello("Alice"));
		System.out.println(hello.Hello("X"));
		
	}

}
